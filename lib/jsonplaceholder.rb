require "jsonplaceholder/version"
require "jsonplaceholder/models"
require "jsonplaceholder/model_api"
require "net/http"
require "uri"

class Jsonplaceholder
  include Jsonplaceholder::ModelApi

  def http_client
    @http_client ||=
      begin
        uri = URI(base_uri)
        Net::HTTP.start(uri.hostname, uri.port)
      end
  end

  def base_uri
    "http://jsonplaceholder.typicode.com/"
  end
end
