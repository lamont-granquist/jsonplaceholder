require 'jsonplaceholder/model'

class Jsonplaceholder
  class Model
    class User < Jsonplaceholder::Model
      type :user

      route "/users/:id"

      json_field :id
      json_field :name
      json_field :username
      json_field :email
      json_field :address
      json_field :phone
      json_field :website
      json_field :company
    end
  end
end
