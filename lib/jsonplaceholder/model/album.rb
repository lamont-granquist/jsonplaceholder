require 'jsonplaceholder/model'

class Jsonplaceholder
  class Model
    class Album < Jsonplaceholder::Model
      type :album

      route "/albums/:id"

      json_field :id
      json_field :title
      json_field :user_id, as: "userId"
    end
  end
end
