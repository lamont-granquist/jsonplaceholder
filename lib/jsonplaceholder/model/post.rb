require 'jsonplaceholder/model'

class Jsonplaceholder
  class Model
    class Post < Jsonplaceholder::Model
      type :post

      route "/posts/:id"

      json_field :id
      json_field :title
      json_field :body
      json_field :user_id, as: "userId"
    end
  end
end
