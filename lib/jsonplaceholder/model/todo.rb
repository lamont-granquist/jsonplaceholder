require 'jsonplaceholder/model'

class Jsonplaceholder
  class Model
    class Todo < Jsonplaceholder::Model
      type :todo

      route "/todos/:id"

      json_field :id
      json_field :title
      json_field :completed
      json_field :user_id, as: "userId"
    end
  end
end
