require 'jsonplaceholder/model'

class Jsonplaceholder
  class Model
    class Comment < Jsonplaceholder::Model
      type :comment

      route "/comments/:id"

      json_field :id
      json_field :name
      json_field :email
      json_field :body
      json_field :post_id, as: "postId"
    end
  end
end
