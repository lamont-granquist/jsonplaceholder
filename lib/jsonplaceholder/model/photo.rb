require 'jsonplaceholder/model'

class Jsonplaceholder
  class Model
    class Photo < Jsonplaceholder::Model
      type :photo

      route "/photos/:id"

      json_field :id
      json_field :title
      json_field :url
      json_field :thumbnailUrl
      json_field :album_id, as: "albumId"
    end
  end
end
