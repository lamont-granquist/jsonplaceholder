require 'ffi_yajl'
require 'jsonplaceholder/model_api'

class Jsonplaceholder
  class Model
    extend Forwardable

    NULL = Object.new

    def_delegators :@api, :base_uri

    # FIXME:  this file has at least 3 concerns:
    #  - subclass DSL + routing and such
    #  - http API like get()
    #  - from_hash/from_json object inflation concerns

    class << self
      attr_accessor :json_fields
      attr_accessor :remap

      def json_field(name, **opts)
        attr_accessor name
        @json_fields ||= []
        @json_fields << name
        @remap ||= {}
        @remap[opts[:as]] = name if opts[:as]
      end

      attr_accessor :route

      def type(sym)
        Jsonplaceholder::ModelApi.add_builder(sym, self)
      end

      def route(r = NULL)
        @route = r unless r.equal?(NULL)
        @route
      end

      def from_json(api, json)
        from_hash(api, FFI_Yajl::Parser.parse(json))
      end

      def from_hash(api, hash)
        obj = new(api)
        hash.each do |key, value|
          key = remap[key] if remap[key]
          if json_fields.include?(key.to_sym)
            obj.send(:"#{key}=", value)
          end
        end
        obj
      end

      def get(api, *args, &block)
        response = api.http_client.get(interpolate_args(*args), &block)
        from_json(api, response.body)
      end

      private

      def interpolate_args(*args)
        args.each_with_object(route.dup) do |arg, memo|
          memo.sub!(/:[^\/]+/, arg.to_s)
        end
      end
    end

    def json_fields
      self.class.json_fields
    end

    def route(*args)
      self.class.route(*args)
    end

    attr_accessor :api

    def initialize(api)
      @api = api
    end

    def from_json(json)
      self.class.from_json(api, json)
    end

    def from_hash(hash)
      self.class.from_hash(api, hash)
    end

    def get(api, *args, &block)
      self.class.get(api, *args, &block)
    end

    private

  end
end
