class Jsonplaceholder
  class ModelBuilder
    attr_accessor :type
    attr_accessor :api
    attr_accessor :klass

    def initialize(api, type, klass)
      @api = api
      @type = type
      @klass = klass
    end

    def get(*args, &block)
      klass.get(api, *args, &block)
    end
  end
end
