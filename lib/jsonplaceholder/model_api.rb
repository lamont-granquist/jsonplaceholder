require 'jsonplaceholder/model_builder'

class Jsonplaceholder
  module ModelApi
    # this class is for the "dynamically" defined methods off the api that
    # are injected by the models
    def self.add_builder(type, klass)
      define_method type do
        Jsonplaceholder::ModelBuilder.new(self, type, klass)
      end
    end
  end
end
