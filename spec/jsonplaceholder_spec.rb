require 'spec_helper'

describe Jsonplaceholder do
  let(:api) do
    Jsonplaceholder.new
  end

  it "gets a list of posts" do
    pending "not implemented"
    expect(api.posts).not_to be nil
  end

  it "gets a single post" do
    p = api.post.get(1)
    expect(p).to be_an_instance_of(Jsonplaceholder::Model::Post)
    expect(p.id).to eql(1)
    expect(p.title).to eql("sunt aut facere repellat provident occaecati excepturi optio reprehenderit")
    expect(p.user_id).to eql(1)
  end

  it "gets a single comment" do
    p = api.comment.get(1)
    expect(p).to be_an_instance_of(Jsonplaceholder::Model::Comment)
    expect(p.id).to eql(1)
    expect(p.name).to eql("id labore ex et quam laborum")
    expect(p.email).to eql("Eliseo@gardner.biz")
    expect(p.post_id).to eql(1)
  end
end
